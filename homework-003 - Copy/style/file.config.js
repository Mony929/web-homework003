// { @import ('tainwind.config.js').Config}

// module.exports = {
//     theme:{
//         extends:{
//             backgroundImage:{
//                 'bg-img': "url('../images/bg-image.avif')"
//             }
//         }
//     }
//

tailwind.config = {
    theme: {
        extend: {
            colors: {
                "text_color": "#ECF0F1"
            },
            backgroundImage:{
                "bg_image" : "url('/images/bg_img1.jpg')"
            },
            backgroundColor:{
                "bg_color1": "#F08080"
            }
        }
    }
}

// /** @type {import('tailwindcss').Config} */
// module.exports = {
//     content: ["./src/**/*.{html,js}"],
//     theme: {
//       extend: {},
//     },
//     plugins: [],
//   }