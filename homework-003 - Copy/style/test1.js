
// var start_time = true;
// var stop_time = false;
// var clear = false;

// before condition

var date_time = setInterval(dateTime, 1000)

function dateTime() {
    var date = new Date();

    document.getElementById("current-date").innerHTML ='<img src="/icons/icons_clock.png" alt="">' +date.toDateString() + " " + date.toLocaleTimeString();;
}

// start time 
// function start_time() {
//     var date1 = new Date();
//     // var d1 = date1.toLocaleTimeString();
//     document.getElementById("start-time").innerHTML = "Start at " + date1.toLocaleTimeString();;
// }

// stop time
// function stop_time(){
//     var date2 = new Date();
//     var d1 = date2.toLocaleTimeString();
//     document.getElementById("start-time").innerHTML = "Start at "+d1;
//     alert("hello")
// }


var btnStart = true;
var btnStop = false;
var btnClear = false;
var  startTime;
var stopTime;
var price;

function start_time() {
    if (btnStart) {
        startTime = new Date();
        document.getElementById("start-time").innerHTML = "Start at " + startTime.toLocaleTimeString();;

        document.getElementById("btn-start").innerHTML = ' <img src="/icons/icons_clock.png" alt=""> Stop'
        document.getElementById('btn-start').style.backgroundColor = 'red'
        btnStart = false;
        btnStop = true;
    }
    else if(btnStop){
        var stopTime = new Date();
        document.getElementById("stop-time").innerHTML = "Stop at " + stopTime.toLocaleTimeString();;

        document.getElementById("btn-start").innerHTML = 'Clear'
        document.getElementById('btn-start').style.backgroundColor = 'orange'

        // if() else
         var minute_play = (stopTime.getHours()*60 + stopTime.getMinutes())-(startTime.getHours()*60 +  startTime.getMinutes());
        // var minute_play = 190;
        if(minute_play <= 15){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "500 Riels"
        }
        else if(minute_play<=30){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "1000 Riels"
        }
        else if(minute_play<=60){
            document.getElementById('minutes').innerHTML = minute_play + "Minute(s)";
            document.getElementById("price").innerHTML = "1500 Riels"
        }else{
            var min = minute_play%60;
            var over_min= Math.trunc(minute_play/60)
            if(min>0 && min <=15){
                price = over_min*1500 + 500;
            }else if(min>0 && min<=30){
                price = over_min*1500 + 1000;
            }else if(min>0 && min < 60){
                price = over_min*1500 + 1500;
            }else{
                price = over_min*1500; 
            }
            document.getElementById("minutes").innerHTML = minute_play + " Minute(s)"
            document.getElementById('price').innerHTML = price + "Riel(s)";
        }

        btnStop = false;
        btnClear = true;
    }
    else{
        var date = new Date();

        document.getElementById("start-time").innerHTML = "Start at 0:00";
        document.getElementById("stop-time").innerHTML = "Stop at 0:00";
        document.getElementById("minutes").innerHTML = "0 minute(s)";
        document.getElementById("price").innerHTML = "0 Riel(s)";
        document.getElementById("btn-start").innerHTML = "Start"
        btnClear = false;
        btnStart = true;
    }

}